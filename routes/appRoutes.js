'use strict';
module.exports = function(app) {
  var deviceTypes = require('../controller/deviceTypeController');
  var devicesUnderTest = require('../controller/deviceUnderTestController')

 // DeviceTypes routes
  app.route('/compoundDeviceTypes')
     .get(deviceTypes.list_compound_device_types);

  app.route('/simpleDeviceTypes')
     .get(deviceTypes.list_simple_device_types);

  app.route('/compoundDeviceComponents/:deviceId')
     .get(deviceTypes.list_compound_device_component_device_types);

  // devicesUnderTest routes
  app.route('/devicesUnderTest/')
    .get(devicesUnderTest.get_devices_under_test);

    app.route('/deviceComponentsUnderTest/:parentId')
      .get(devicesUnderTest.get_device_components_under_test);

  app.route('/deviceUnderTest/:deviceTypeId')
    .post(devicesUnderTest.add_new_device);

  app.route('/updateDeviceSerialNumber/:deviceId/:serialNumber')
    .put(devicesUnderTest.set_serial_number);

  app.route('/updateComponentSerialNumber/:parentId/:componentName/:serialNumber')
    .put(devicesUnderTest.set_component_serial_number);

  app.route('/deviceTests/:deviceId')
    .get(devicesUnderTest.get_device_tests);

  app.route('/deviceTest/:deviceId/:test_id/:result')
    .put(devicesUnderTest.update_device_test);
};
