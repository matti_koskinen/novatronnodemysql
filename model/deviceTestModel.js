'user strict';
var sql = require('./db.js');

//DeviceType object constructor
var DeviceTest = function() {
}

DeviceTest.addTest = function addTest(device_id, test_id, result)
{
  var params = [device_id, test_id];
  sql.query("INSERT INTO device_tests (`device_id`, `test_id`) VALUES (?,?)", params, function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      result(null, res);
    }
  });
}

DeviceTest.getDeviceTypeTests = function getDeviceTypeTests(deviceTypeId, result) {
  sql.query("SELECT test_id FROM device_type_test WHERE device_type_id = ?", deviceTypeId, function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      result(null, res);
    }
  });
}

module.exports = DeviceTest;
