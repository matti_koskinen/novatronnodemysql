'user strict';
var sql = require('./db.js');

//DeviceType object constructor
var DeviceType = function() {
}

DeviceType.getCompoundDeviceTypes = function getCompundDeviceTypes(result)
{
  sql.query("SELECT * FROM react_sql.device_types WHERE compound_device = 1", function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      result(null, res);
    }
  });
}

DeviceType.getSimpleDeviceTypes = function getCompundDeviceTypes(result)
{
  sql.query("SELECT * FROM react_sql.device_types WHERE compound_device = 0", function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      result(null, res);
    }
  });
}

DeviceType.getComponentsById = function getComponentsById(deviceId, result) {
  sql.query("SELECT compound_device_components.compound_device_type_id, compound_device_components.component_id, compound_device_components.component_name, device_types.device_type_id, device_types.device_type_name FROM compound_device_components \
  INNER JOIN device_types ON compound_device_components.device_type_id = device_types.device_type_id \
  WHERE compound_device_components.compound_device_type_id  = ?", deviceId, function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(err, null);
    }
    else{
      result(null, res);
    }
  });
};

DeviceType.getCompoundDeviceComponents = function getCompoundDeviceComponents(deviceTypeId, result) {
  sql.query("SELECT * FROM compound_device_components WHERE compound_device_type_id = ?", deviceTypeId, function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      result(null, res);
    }
  });
}


module.exports = DeviceType;
