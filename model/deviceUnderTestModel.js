'user strict';
var sql = require('./db.js');

function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

Date.prototype.toMysqlFormat = function() {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};

//DeviceType object constructor
var DeviceUnderTest = function() {
}

DeviceUnderTest.addDevice = function addDevice(device_type, result) {
  sql.query("INSERT INTO devices_under_test (`device_type`) VALUES (?)", device_type, function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      console.log('device : ', res);
      console.log('device : ', res.insertId);
      result(null, res);
    }
  });
}

DeviceUnderTest.addComponentDevice = function addComponentDevice(device_type, parent_id, component_id, component_name, result) {
  var params = [device_type, parent_id, component_id, component_name];

  sql.query("INSERT INTO devices_under_test (`device_type`, `parent_id`, `component_id`, `component_name`) VALUES (?,?,?,?)",
    params, function (err, res) {
    if (err) {
      console.log("error: ", err);
      result(null, err);
    }
    else {
      result(null, res);
    }
  });
}

DeviceUnderTest.setSerialNumber = function setSerialNumber(device_id, serial_number, result) {
  var params = [serial_number, device_id];

  sql.query("UPDATE devices_under_test SET serial_number = ? WHERE  device_id = ?", params, function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      result(null, res);
    }
  });
}


DeviceUnderTest.setComponentSerialNumber = function setComponentSerialNumber(parent_id, component_name, serial_number, result) {
  var params = [serial_number, parent_id, component_name];

  sql.query("UPDATE devices_under_test SET serial_number = ? WHERE  parent_id = ? AND component_name = ?", params, function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      result(null, res);
    }
  });
}

DeviceUnderTest.getDevicesUnderTest = function getDevicesUnderTest(result) {
  sql.query("SELECT * FROM devices_under_test WHERE parent_id IS null", function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      result(null, res);
    }
  });
}

DeviceUnderTest.getDeviceComponentsUnderTest = function getDeviceComponentsUnderTest(parent_id, result) {
  sql.query("SELECT * FROM devices_under_test WHERE parent_id = ?", parent_id, function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      result(null, res);
    }
  });
}

DeviceUnderTest.getDeviceTests = function getDeviceTests(device_id, result) {
  sql.query("SELECT test.test_name, device_tests.execution_day, device_tests.result, device_tests.executed_by, test.test_description \
    FROM device_tests \
    INNER JOIN test ON device_tests.test_id=test.test_id \
    WHERE device_id = ?", device_id, function (err, res) {
      if(err) {
        console.log("error: ", err);
        result(null, err);
      }
      else{
        result(null, res);
      }
  });
}

DeviceUnderTest.updateDeviceTest = function updateDeviceTest(device_id, test_id, test_result, result) {
  var execution_day = new Date();
  var executed_by = sql.config.user;
  console.log(sql.config.user);

  var params = [test_result, execution_day.toMysqlFormat(), executed_by, device_id, test_id];
  sql.query("UPDATE device_tests SET result = ?, execution_day = ?, executed_by = ? WHERE device_id = ? AND test_id = ?", params, function (err, res) {
      if(err) {
        console.log("error: ", err);
        result(null, err);
      }
      else{
        result(null, res);
      }
  });
}


module.exports = DeviceUnderTest;
