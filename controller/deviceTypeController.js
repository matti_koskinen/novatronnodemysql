'use strict';

var DeviceType = require('../model/deviceTypeModel.js');

exports.list_compound_device_types = function(req, res) {
  DeviceType.getCompoundDeviceTypes(function(err, task) {

    console.log('controller')
    if (err)
      res.send(err);
      console.log('res', task);
    res.send(task);
  });
}

exports.list_simple_device_types = function(req, res) {
  DeviceType.getSimpleDeviceTypes(function(err, task) {

    console.log('controller')
    if (err)
      res.send(err);
      console.log('res', task);
    res.send(task);
  });
}

exports.list_compound_device_component_device_types = function(req, res) {
  DeviceType.getComponentsById(req.params.deviceId, function(err, task) {

    console.log('controller')
    if (err)
      res.send(err);
      console.log('res', task);
    res.send(task);
  });
}
