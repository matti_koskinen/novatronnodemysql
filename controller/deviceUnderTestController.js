'use strict';

var DevicesUnderTest = require('../model/deviceUnderTestModel.js');
var DeviceTypes = require('../model/deviceTypeModel.js');
var DeviceTests = require('../model/deviceTestModel.js');

exports.add_new_device = function(req, res) {

  var parentId;

  DevicesUnderTest.addDevice(req.params.deviceTypeId, function(err, result) {
    if (err) {
      console.log("error: ", err);
      res.send(err);
    }
    parentId = result.insertId;
    console.log("parentId: ", parentId);

    DeviceTests.getDeviceTypeTests(req.params.deviceTypeId, function(err, result) {
      if (err) {
        console.log("error: ", err);
        res.send(err);
      }
      result.forEach(function(test) {
        console.log("test", test);
        DeviceTests.addTest(parentId, test.test_id, function(err, result) {
          if (err)
            console.log("error: ", err);
        });
      });
    });

    DeviceTypes.getCompoundDeviceComponents(req.params.deviceTypeId, function(err, result) {
      if (err) {
        console.log("error: ", err);
        res.send(err);
      }

      result.forEach(function(component) {
        DevicesUnderTest.addComponentDevice(component.device_type_id, parentId, component.component_id, component.component_name, function(err, result) {
          if (err)
            console.log("error: ", err);
          var childId = result.insertId;
          console.log("childId", childId);

          DeviceTests.getDeviceTypeTests(component.device_type_id, function(err, result) {
            if (err) {
              console.log("error: ", err);
              res.send(err);
            }
            result.forEach(function(test) {
              console.log("test", test);
              DeviceTests.addTest(childId, test.test_id, function(err, result) {
                if (err)
                  console.log("error: ", err);
              });
            });
          });
        });
      });
    });
    console.log('res', result);
    res.send(result);
  });
}

exports.set_serial_number = function(req, res) {
  DevicesUnderTest.setSerialNumber(req.params.deviceId, req.params.serialNumber, function(err, result) {
    if (err) {
      console.log("error: ", err);
      res.send(err);
    }
    console.log('res', result);
    res.send(result);
  });
}

exports.set_component_serial_number = function(req, res) {
  DevicesUnderTest.setComponentSerialNumber(req.params.parentId, req.params.componentName, req.params.serialNumber, function(err, result) {
    if (err) {
      console.log("error: ", err);
      res.send(err);
    }
    console.log('res', result);
    res.send(result);
  });
}

exports.get_devices_under_test = function(req, res) {
  DevicesUnderTest.getDevicesUnderTest( function(err, result) {
    if (err) {
      console.log("error: ", err);
      res.send(err);
    }
    console.log('res', result);
    res.send(result);
  });
}

exports.get_device_components_under_test = function(req, res) {
  DevicesUnderTest.getDeviceComponentsUnderTest(req.params.parentId, function(err, result) {
    if (err) {
      console.log("error: ", err);
      res.send(err);
    }
    console.log('res', result);
    res.send(result);
  });
}

exports.get_device_tests = function(req, res) {
  DevicesUnderTest.getDeviceTests(req.params.deviceId, function(err, result) {
    if (err) {
      console.log("error: ", err);
      res.send(err);
    }
    console.log('res', result);
    res.send(result);
  });
}

exports.update_device_test = function(req, res) {
  DevicesUnderTest.updateDeviceTest(req.params.deviceId, req.params.test_id, req.params.result, function(err, result) {
    if (err) {
      console.log("error: ", err);
      res.send(err);
    }
    console.log('res', result);
    res.send(result);
  });
}
